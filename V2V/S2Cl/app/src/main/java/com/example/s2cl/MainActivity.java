package com.example.s2cl;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class MainActivity extends Activity {

    private final int SERVERPORT = 9000;

    private Button connectPhones, upfront, backwards, left, right, stop, emergency, accident, technical, towed;

    private EditText serverIp;
    private String serverIpAddress ;
    private boolean connected = false;

    private Handler handler = new Handler();
    private Socket clientSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serverIp = (EditText) findViewById(R.id.server_ip);
        connectPhones = (Button) findViewById(R.id.connect_phones);

        upfront = (Button) findViewById(R.id.up);
        backwards = (Button) findViewById(R.id.down);
        left = (Button) findViewById(R.id.left);
        right = (Button) findViewById(R.id.right);
        stop = (Button) findViewById(R.id.stop);

        emergency = (Button) findViewById(R.id.emergency);
        accident = (Button) findViewById(R.id.accident);
        technical = (Button) findViewById(R.id.technical);
        towed = (Button) findViewById(R.id.towed);


        connectPhones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!connected) {
                    serverIpAddress = serverIp.getText().toString();
                    if (!serverIpAddress.equals("")) {
                        Thread cThread = new Thread(new ClientThread());
                        cThread.start();
                    }
                }
            }
        });

        upfront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Inainte", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("Upfront");
                    Log.d("Inainte", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Inainte", "S: Error", e);
                }

            }

        });

        backwards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Spate", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("Backwards");
                    Log.d("Spate", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Spate", "S: Error", e);
                }
            }

        });

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Stangaa", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("Left");
                    Log.d("Stangaa", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Stangaa", "S: Error", e);
                }
            }
        });


        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Dreapta", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("Right");
                    Log.d("Dreapta", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Dreapta", "S: Error", e);
                }
            }
        });


         stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Stop", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("Stop");
                    Log.d("Stop", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Stop", "S: Error", e);
                }
            }
        });


        emergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Emergency message", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("Please clear the way");
                    Log.d("Emergency message", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Emergency message", "S: Error", e);
                }
            }
        });

        accident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Accident message", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("Accident ahead");
                    Log.d("Accident message", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Accident message", "S: Error", e);
                }
            }
        });

        technical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Technical message", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("Technical problems");
                    Log.d("Technical message", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Technical message", "S: Error", e);
                }
            }
        });

        towed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d("Car assistance message", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                    out.println("I need assistance");
                    Log.d("Car assistance message", "C: Sent.");
                } catch (Exception e) {
                    Log.e("Car assistance message", "S: Error", e);
                }
            }
        });

    }

        public class ClientThread implements Runnable {

        public void run() {
            try {
                InetAddress serverAddr = InetAddress.getByName(serverIpAddress);
                Log.d("ClientThread", "C: Connecting...");
                clientSocket = new Socket(serverAddr, SERVERPORT);

                connected = true;
                if(connected) {
                    try {
                        Log.d("ClientThread", "C: Sending command.");
                        PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);

                        out.println("Hey Server!");
                        Log.d("ClientThread", "C: Sent.");

                    } catch (Exception e) {
                        Log.e("ClientThread", "S: Error", e);
                    }
                }
            } catch (Exception e) {
                Log.e("ClientThread", "C: Error", e);
                connected = false;
            }
        }
    }


}
