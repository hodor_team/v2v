package com.example.s2;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

import me.palazzetti.adktoolkit.AdkManager;

public class MainActivity extends Activity {


    private AdkManager mAdkManager;
    //private AdkReadTask mAdkReadTask;

    private TextView serverStatus;
    private EditText textcon;
    private EditText textcon1;
    private String p;


    private EditText distance1;
    private EditText distance2;

    // default ip
    public String SERVERIP;

    // designate a port
    public  final int SERVERPORT = 9000;

    private Handler handler = new Handler();
    private String line = null;

    private ServerSocket serverSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //starting the adk
        mAdkManager = new AdkManager((UsbManager) getSystemService(Context.USB_SERVICE));

        //register a BroadcastReceiver to catch UsbManager.ACTION_USB_ACCESSORY_DETACHED action
        registerReceiver(mAdkManager.getUsbReceiver(), mAdkManager.getDetachedFilter());

        serverStatus = (TextView) findViewById(R.id.server_status);
        textcon = (EditText) findViewById(R.id.text);
        textcon1 = (EditText) findViewById(R.id.text1);
        distance1 = (EditText) findViewById(R.id.editText1);
        distance2 = (EditText) findViewById(R.id.editText2);

        SERVERIP = getLocalIpAddress();

        Thread fst = new Thread(new ServerThread());
        fst.start();


    }

    public class ServerThread implements Runnable {

        public void run() {
            try {
                if (SERVERIP != null) {

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            serverStatus.setText("I'm here: " + SERVERIP);
                        }
                    });

                    serverSocket = new ServerSocket(SERVERPORT);
                    while (true) {


                        Socket client = serverSocket.accept();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                serverStatus.setText("Connected.");
                            }
                        });

                        try {
                            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));

                            while((line = in.readLine()) != null) {
                                Log.d("ServerActivity", line);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        textcon.append(line);
                                        p = textcon.getText().toString();

                                        textcon1.setText(p);
                                        textcon.setText("");
                                        Log.d("ceva", p);
                                        switch(p) {
                                            case "Upfront" :
                                                            Log.d("ceva", "up");
                                                            mAdkManager.writeSerial("1");
                                                            break;
                                            case "Backwards":
                                                            Log.d("ceva", "backwards");
                                                            mAdkManager.writeSerial("2");
                                                            break;
                                            case "Right" :
                                                            Log.d("ceva", "right");
                                                            mAdkManager.writeSerial("3");
                                                            break;
                                            case "Left"  :
                                                            Log.d("ceva", "left");
                                                            mAdkManager.writeSerial("4");
                                                            break;
                                            case "Stop"  :
                                                            Log.d("ceva", "stop");
                                                            mAdkManager.writeSerial("0");
                                                            break;



                                        }
                                    }
                                });
                            }
                            break;
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    serverStatus.setText("Oops. Connection interrupted. Please reconnect your phones.");
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            serverStatus.setText("Couldn't detect internet connection.");
                        }
                    });
                }
            } catch (Exception e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        serverStatus.setText("Error");
                    }
                });
                e.printStackTrace();
            }
        }
    }

    // gets the ip address of your phone's network
    private String getLocalIpAddress() {
        WifiManager wifiMgr = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        String ipAddress = Formatter.formatIpAddress(ip);

        return ipAddress;
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            // make sure you close the socket upon exiting
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void onResume() {
        super.onResume();
        mAdkManager.open();

        //mAdkReadTask = new AdkReadTask();
       // mAdkReadTask.execute();


    }

    @Override
    public void onPause() {
        super.onPause();
        mAdkManager.close();

       // mAdkReadTask.pause();
       // mAdkReadTask = null;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mAdkManager.getUsbReceiver());
    }


    /*
    private class AdkReadTask extends AsyncTask<Void, String, Void> {

        private boolean running = true;

        public void pause(){
            running = false;
        }

        protected Void doInBackground(Void... params) {
            while(running) {
                publishProgress(mAdkManager.readSerial()) ;
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

            distance1.setText( (int) progress[0].charAt(0)+ "cm");
            distance2.setText((int) progress[0].charAt(1)+ "cm");

        }
    }*/



}