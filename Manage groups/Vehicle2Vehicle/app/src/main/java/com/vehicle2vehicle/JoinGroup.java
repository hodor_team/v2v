package com.vehicle2vehicle;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class JoinGroup extends Activity {

    private EditText name, password;
    private TextView tvname,tv1,tv3,tv2;
    private Button create,refreshButton;
    private Switch swt;
    private String data="";
    private String[] participants=new String[3];
    private ImageView img1,img2,img3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_group);
        name=(EditText)findViewById(R.id.join_group_name);
        password=(EditText)findViewById(R.id.join_group_password);
        tvname=(TextView)findViewById(R.id.join_tvname);
        tv1=(TextView)findViewById(R.id.join_textView3);
        tv2=(TextView)findViewById(R.id.join_textView2);
        tv3=(TextView)findViewById(R.id.join_textView1);
        img1=(ImageView)findViewById(R.id.join_createGroupImageViewRound1);
        img2=(ImageView)findViewById(R.id.join_createGroupImageViewRound2);
        img3=(ImageView)findViewById(R.id.join_createGroupImageViewRound3);
        create=(Button)findViewById(R.id.join_createButton);
        refreshButton=(Button)findViewById(R.id.join_refreshButton);
        swt=(Switch)findViewById(R.id.join_switch1);
        invisibleForAll();
        swt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if(!isChecked){
                    tvname.setText("Name: ");
                    callFromServer(readFromFile("id_user.txt"),"http://" + DataIpAdress.ipAddress +"/vehicle/DeleteUserFromGroup.php");
                    invisibleForAll();
                }

            }
        });
    }

    public void invisibleForAll(){
        tv1.setVisibility(View.INVISIBLE);
        tv2.setVisibility(View.INVISIBLE);
        tv3.setVisibility(View.INVISIBLE);
        img1.setVisibility(View.INVISIBLE);
        img2.setVisibility(View.INVISIBLE);
        img3.setVisibility(View.INVISIBLE);
    }

    public void refresh(View view) {
        new Thread(new Runnable() {
            public void run() {
                data = get_from_server();
                participants = parseJSON(data);
                if(participants[0].length()!=0)
                    tv1.post(new Runnable() {
                        public void run() {
                            tv1.setText(participants[0]); tv1.setVisibility(View.VISIBLE);
                        }
                    });
                img1.post(new Runnable() {
                    public void run() {
                        img1.setVisibility(View.VISIBLE);
                    }
                });
                try{
                    if(participants[1].length()!=0)
                        tv2.post(new Runnable() {
                            public void run() {
                                tv2.setText(participants[1]);tv2.setVisibility(View.VISIBLE);
                            }
                        });
                    img2.post(new Runnable() {
                        public void run() {
                            img2.setVisibility(View.VISIBLE);
                        }
                    });
                }catch (Exception e) {
                    e.printStackTrace();
                    tv2.post(new Runnable() {
                        public void run() {
                            tv2.setText(""); tv2.setVisibility(View.INVISIBLE);
                        }
                    });
                    img2.post(new Runnable() {
                        public void run() {
                            img2.setVisibility(View.INVISIBLE);
                        }
                    });
                }
                try{
                    if(participants[2].length()!=0)
                        tv3.post(new Runnable() {
                            public void run() {
                                tv3.setText(participants[2]); tv3.setVisibility(View.VISIBLE);
                            }
                        });
                    img3.post(new Runnable() {
                        public void run() {
                            img3.setVisibility(View.VISIBLE);
                        }
                    });
                }catch (Exception e) {
                    e.printStackTrace();
                    tv3.post(new Runnable() {
                        public void run() {
                            tv3.setText(""); tv3.setVisibility(View.INVISIBLE);
                        }
                    });
                    img3.post(new Runnable() {
                        public void run() {
                            img3.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        }).start();
    }


    public void joinGroup(final View view) {
        new Thread(new Runnable() {
            public void run() {
                String dataResponse = send_to_server(name.getText().toString(),password.getText().toString(),readFromFile("id_user.txt"));
                String groupId = parseJoinJSON(dataResponse);
                if(groupId.length()==0){
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast toast = Toast.makeText(view.getContext(), "No such group. Please retry!", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    });
                }
                else{
                    tvname.post(new Runnable() {
                        public void run() {
                            tvname.setText("Name: "+name.getText().toString());
                        }
                    });
                    tv1.post(new Runnable() {
                        public void run() {
                            tv1.setText(readFromFile("email_user.txt")); tv1.setVisibility(View.VISIBLE);
                        }
                    });
                    img1.post(new Runnable() {
                        public void run() {
                            img1.setVisibility(View.VISIBLE);
                        }
                    });
                    name.post(new Runnable() {
                        public void run() {
                            name.setText(null);
                        }
                    });
                    password.post(new Runnable() {
                        public void run() {
                            password.setText(null);
                        }
                    });
                    swt.post(new Runnable() {
                        public void run() {
                            swt.setChecked(true);
                        }
                    });

                }
            }
        }).start();
    }

    private String send_to_server(final String name, final String password, final String id_user)
    {
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://" + DataIpAdress.ipAddress +"/vehicle/JoinGroup.php");

            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("name",name));
                nameValuePairs.add(new BasicNameValuePair("password",password));
                nameValuePairs.add(new BasicNameValuePair("id_user",id_user));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                String response = httpclient.execute(httppost,
                        responseHandler);

                return response.trim();

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error";
    }

    private String readFromFile(String file) {
        String ret = "";
        try {
            InputStream inputStream = this.openFileInput(file);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                try {
                    while ( (receiveString = bufferedReader.readLine()) != null ) {
                        stringBuilder.append(receiveString);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public String[] parseJSON(String result) {
        String[] names  = new String[100];
        try {
            JSONObject jsonResponse = new JSONObject(result);
            JSONArray jsonMainNode = jsonResponse.optJSONArray("participants");
            for (int i = 0; i < jsonMainNode.length() ; i++) {
                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                String name = jsonChildNode.getString("email");
                names[i]=name;
                Log.d("name: ", name);
            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return  names;
    }
    private String get_from_server()
    {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://"+ DataIpAdress.ipAddress +"/vehicle/GetGroupParticipants.php");
            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("name",tvname.getText().toString().replace("Name: ","")));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                String response = httpclient.execute(httppost,
                        responseHandler);

                return response.trim();

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error";
    }

    public String parseJoinJSON(String result) {
        String response = "";
        try {
            JSONObject json_data =  new JSONObject(result);
            response = json_data.getString("id");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return  response;
    }

    private void callFromServer(final String id, final String path)
    {
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(path);
                    try {
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                        nameValuePairs.add(new BasicNameValuePair("id_user",id));
                        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        httpclient.execute(httppost);
                    } catch (ClientProtocolException e) {
                        // TODO Auto-generated catch block
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });thread.start();
    }
}





