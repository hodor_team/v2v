package com.vehicle2vehicle;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.test.ActivityUnitTestCase;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SignUp extends Activity {

    private EditText email,password,username;
    private Button signUp;
    private ImageView imageViewRound;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        username = (EditText) findViewById(R.id.etUserName);
        password = (EditText) findViewById(R.id.etPass);
        email = (EditText) findViewById(R.id.etEmail);
        signUp = (Button)findViewById(R.id.btnRegister);
        imageViewRound=(ImageView)findViewById(R.id.signUpImageViewRound);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.usericon1);

    }

    public void register(View view) {

        send_to_server(email.getText().toString() ,password.getText().toString(),username.getText().toString()  );
        Toast toast = Toast.makeText(this.getApplicationContext(), "You have succesfully Signed Up. Please Log in with your credentials", Toast.LENGTH_LONG);
        toast.show();

        Intent intent = new Intent(this, Start.class);
        startActivity(intent);
    }

    private void send_to_server(final String email, final String password, final String username)
    {
        Thread thread = new Thread(new Runnable(){
            @Override

            public void run() {
                try {


                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost("http://" + DataIpAdress.ipAddress + "/vehicle/SignUp.php");

                    try {
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                        nameValuePairs.add(new BasicNameValuePair("email",email));
                        nameValuePairs.add(new BasicNameValuePair("password",password));
                        nameValuePairs.add(new BasicNameValuePair("username",username));

                        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        httpclient.execute(httppost);

                    } catch (ClientProtocolException e) {
                        // TODO Auto-generated catch block
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });thread.start();
    }
}




