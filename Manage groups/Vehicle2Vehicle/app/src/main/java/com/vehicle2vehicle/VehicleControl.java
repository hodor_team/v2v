package com.vehicle2vehicle;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.os.AsyncTask;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.palazzetti.adktoolkit.AdkManager;


public class VehicleControl extends Activity {

    //private AdkReadTask mAdkReadTask;
    ImageButton up, down, left, right, stop;
    String  stringUp    ="up",
            stringDown  ="down",
            stringLeft  ="left",
            stringRight ="right",
            stringStop = "stop";


    private AdkManager mAdkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_control);

        //starting the adk
        mAdkManager = new AdkManager((UsbManager) getSystemService(Context.USB_SERVICE));

        //register a BroadcastReceiver to catch UsbManager.ACTION_USB_ACCESSORY_DETACHED action
        registerReceiver(mAdkManager.getUsbReceiver(), mAdkManager.getDetachedFilter());

        up = (ImageButton)findViewById(R.id.buttonUp);
        setOnButtonClickListener(stringUp, up);

        down = (ImageButton)findViewById(R.id.buttonDown);
        setOnButtonClickListener(stringDown, down);

        left = (ImageButton)findViewById(R.id.buttonLeft);
        setOnButtonClickListener(stringLeft, left);

        right = (ImageButton)findViewById(R.id.buttonRight);
        setOnButtonClickListener(stringRight, right);

        stop = (ImageButton)findViewById(R.id.buttonStop);
        setOnButtonClickListener(stringStop, stop);
    }

    private void setOnButtonClickListener(final String type, ImageButton button ){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (type) {
                    case "up":
                        Log.d("ceva", "up");
                        mAdkManager.writeSerial("1");
                        break;

                    case "down":
                        Log.d("ceva", "down");
                        mAdkManager.writeSerial("2");
                        break;

                    case "right":
                        mAdkManager.writeSerial("3");
                        break;

                    case "left":
                        Log.d("ceva", "left");
                        mAdkManager.writeSerial("4");
                        break;

                    case "stop":
                        Log.d("ceva", "stop");
                        mAdkManager.writeSerial("0");
                        break;
                }
            }
        });

    }


  /*  private void send_to_server(final String up, final String down, final String right, final String left)
    {
        Thread thread = new Thread(new Runnable(){
            @Override

            public void run() {
                try {

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost("http://" + DataIpAdress.ipAddress + "/vehicle/VehicleControl.php");

                    try {
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                        nameValuePairs.add(new BasicNameValuePair("up",up));
                        nameValuePairs.add(new BasicNameValuePair("down",down));
                        nameValuePairs.add(new BasicNameValuePair("right",right));
                        nameValuePairs.add(new BasicNameValuePair("left",left));
                        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        httpclient.execute(httppost);

                    } catch (ClientProtocolException e) {
                        // TODO Auto-generated catch block
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });thread.start();
    } */



    @Override
    public void onResume() {
        super.onResume();
        mAdkManager.open();
    }

    @Override
    public void onPause() {
        super.onPause();
        mAdkManager.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mAdkManager.getUsbReceiver());
    }
}



