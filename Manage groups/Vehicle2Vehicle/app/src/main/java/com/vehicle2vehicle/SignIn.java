package com.vehicle2vehicle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class SignIn extends Activity {

    private EditText email, password;
    private Button signIn;
    private ImageView imageViewRound;
    private String data="";

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);

        password = (EditText) findViewById(R.id.signIn_etPass);
        email = (EditText) findViewById(R.id.signIn_etUserName);
        signIn = (Button)findViewById(R.id.signIn_btnSingIn);
        imageViewRound=(ImageView)findViewById(R.id.imageView_round);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.usericon1);
        imageViewRound.setImageBitmap(icon);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                new Thread(new Runnable() {
                    public void run() {
                        data = get_from_server();
                        System.out.println(data);

                        String s=new String("[]");
                        if(data.equals(s)) {
                            hideKeyboard();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(v.getContext(), "User / Password incorrect", Toast.LENGTH_LONG).show();
                                }
                            });
                            writeToFile("nothing","id_user.txt");
                        }
                        else {
                            String id_user = parseJSON(data);
                            writeToFile(id_user,"id_user.txt");
                            writeToFile(email.getText().toString(),"email_user.txt");
                            // Toast.makeText(v.getContext(), "Welcome!" , Toast.LENGTH_LONG).show();
                            hideKeyboard() ;

                            Intent intent = new Intent(v.getContext(), Home.class);
                            startActivity(intent);
                        }

                    }
                }).start();
            }
        });
    }

    public String parseJSON(String result) {
        String id=new String();
        try {

            JSONObject json_data =  new JSONObject(result);
            id = json_data.getString("id");
            System.out.println("ID: "+id);

        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return id;
    }

    private String readFromFile() {
        String ret = "";
        try {
            InputStream inputStream = this.openFileInput("id_user.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                try {
                    while ( (receiveString = bufferedReader.readLine()) != null ) {
                        stringBuilder.append(receiveString);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return ret;
    }

    private void writeToFile(String data,String name) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.openFileOutput(name, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String get_from_server()
    {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://"+ DataIpAdress.ipAddress + "/vehicle/LogIn.php");
            try {
                String  emailString         =new String(),
                        passwordString      =new String();

                emailString         =(email.getText().toString());
                passwordString      =(password.getText().toString());
                Log.d("pass", emailString + " " + passwordString);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("email", emailString));
                nameValuePairs.add(new BasicNameValuePair("password", passwordString));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                String response = httpclient.execute(httppost,
                        responseHandler);

                return response.trim();

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error";
    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


}
