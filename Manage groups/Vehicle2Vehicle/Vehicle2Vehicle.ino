  // needed for protocol
  #include "variant.h"
  #include <adk.h>

  // size of reading/writing buffer
  #define BUFFSIZE 128
  
  // buffer for read/write op
  //
  uint8_t readingBuffer[BUFFSIZE];
  uint8_t writingBuffer[3];
  uint32_t bytesRead = 0;

  
  // accesory descriptor
  //it has to be the same name of your Android app,
  //othewise application will stop
  char descriptionName[] = "Vehicle2Vehicle"; 
  
  // same as in the usb_accesory_filter.xml
  char versionNumber[] = "0.3.0";  //0.1.0
  char modelName[] = "External-Droid";
  char manufacturerName[] = "Example";
  
  char serialNumber[] = "1";
  char url[] = "http://www.example.com"; 
   
  // used to initialise the communication
  USBHost Usb;

  ADK adk(&Usb, manufacturerName, modelName, descriptionName, versionNumber, url, serialNumber);
 
   
  //pins alloc
  const int ENA = 2;
  const int ENB = 3; 

  const int IN1 = 25; 
  const int IN2 = 24;
  const int IN3 = 26;
  const int IN4 = 27;
  
  int trigPin [2] = {30, 32};   
  int echoPin [2] = {31, 33};
  
  
  void setup() {
  
   // Serial.begin(9600); 
    
    pinMode(ENA, OUTPUT);
    pinMode(ENB, OUTPUT);
    pinMode(IN1, OUTPUT);
    pinMode(IN2, OUTPUT);
    pinMode(IN3, OUTPUT);
    pinMode(IN4, OUTPUT);
    
    for(int i = 0; i < 2; i++) {
      pinMode(trigPin[i], OUTPUT);  
      pinMode(echoPin[i], INPUT);   
    }   
  } 
 
  float calcul (int trigPin, int echoPin) {
   
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    
    //pinMode(echoPin, INPUT);
  
    int duration = pulseIn(echoPin, HIGH);
 
   // convert the time into a distance
    return ((float)(duration/2)) / 29.1; 
  }

 
 void demoForward(){
   analogWrite(ENA, 240); //170
   analogWrite(ENB, 250); //180
            
   //direction
   digitalWrite(IN1, HIGH); //forward A-right
   digitalWrite(IN2, LOW);  //backwards 
   digitalWrite(IN3, LOW);  //backwards B-left
   digitalWrite(IN4, HIGH); //forward B
 }
 
 
 void demoBackwards(){
   analogWrite(ENA, 250); //200
   analogWrite(ENB, 250); //200
        
   //direction
   digitalWrite(IN1, LOW);
   digitalWrite(IN2, HIGH); 
   digitalWrite(IN3, HIGH);  
   digitalWrite(IN4, LOW); 
 }
 
 void demoRight(){
   analogWrite(ENA, 85);  //85
   analogWrite(ENB, 255); 
            
   digitalWrite(IN1, HIGH);  
   digitalWrite(IN2, LOW);   
   digitalWrite(IN3, LOW);   
   digitalWrite(IN4, HIGH);  
 }
 
 void demoLeft(){
   analogWrite(ENA, 255);
   analogWrite(ENB, 85); //85
            
   digitalWrite(IN1, HIGH);
   digitalWrite(IN2, LOW); 
   digitalWrite(IN3, LOW);
   digitalWrite(IN4, HIGH);
 }
 
 void demoStop(){
   analogWrite(ENA, 250); 
   analogWrite(ENB, 250);
   
   digitalWrite(IN1, HIGH);
   digitalWrite(IN2, HIGH);
   digitalWrite(IN3, HIGH);
   digitalWrite(IN4, HIGH); 
 }
 
 void demoFrontAccelerate() {
   for (int i = 0; i < 256; i++) {
    analogWrite(ENA, i);
    analogWrite(ENB, i);
    
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, HIGH); 
    
    delay(20);
  } 
   
 }
 
 
 
  
  void loop() {
  
   Usb.Task();
       
    if (adk.isReady()) { 
     
      for(int i=0; i<2; i++) {
      writingBuffer[i] = (uint8_t) calcul (trigPin[i], echoPin[i]);
 
    }
    
    adk.write(sizeof(writingBuffer), writingBuffer);
    delay(20);
      
       if (writingBuffer[0] < 20) {
      demoStop();
    }
    
    if( writingBuffer[1] < 20) {
       demoStop();
    }
    
          
     
     adk.read(&bytesRead, BUFFSIZE, readingBuffer);
   
    if(bytesRead > 0){} {       
       switch ((readingBuffer[0])) {  
          // 1 -- up
          case '1' :
                    demoForward();
                    break;        
         //2 -- inapoi
         case '2' :        
                    demoBackwards();
                    break;
         //3 -- dreapta
         case '3' :
                  demoRight();
                  break; 
      //4 -- stanga
        case '4' :
                  demoLeft();
                  break;        
       //0 -- stop
       case '0' :
                 demoStop();
                 break;
       } 
       
     } 
     delay (20);
   } //adk is ready
  } //loop
    
