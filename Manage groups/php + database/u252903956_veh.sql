-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: u252903956_veh
-- ------------------------------------------------------
-- Server version	5.1.73
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Cars`
--

DROP TABLE IF EXISTS `Cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cars` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `timestamp` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `blue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `red` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `green` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cars`
--

LOCK TABLES `Cars` WRITE;
/*!40000 ALTER TABLE `Cars` DISABLE KEYS */;
/*!40000 ALTER TABLE `Cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES (1,'name','password'),(11,'1','1'),(3,'hihihu','daniela1234'),(4,'grq','fffff'),(10,'1','1'),(9,'1','1'),(8,'1','1'),(12,'1','1'),(13,'uguu','ggg'),(14,'ghici','gtgt'),(15,'ggg','gg'),(16,'groooo','gtgt'),(17,'hggh','bvh'),(18,'ff','ghg'),(19,'f','j'),(20,'t','c'),(21,'fggu','hfg'),(22,'uhggug','ffff'),(23,'hhyy','ggg'),(24,'',''),(25,'njnjnj','hhhh'),(26,'uuuuiiii','h'),(27,'',''),(28,'',''),(29,'eugeen','eugeen'),(30,'ooo','ooo'),(31,'pppp','llll'),(32,'mergebaa','daniela'),(33,'errt fgdg fhh','my group'),(34,'my group','oooo'),(35,'ffcv',''),(36,'hhh',''),(37,'uhjbj','jbj'),(38,'plo','plo'),(39,'iiii','ioio'),(40,'ujh','ujh'),(41,'aaaa','aaaa'),(42,'ujuj','ujuj'),(43,'group','parola'),(44,'mimi','mimi'),(45,'cata','cata'),(46,'tara','tara'),(47,'hihi','hihi'),(48,'uuuuuu','uuuuuu'),(49,'mko!lp','mko!lp'),(50,'off','off'),(51,'hw','hw'),(52,'qqqq','qqqq'),(53,'',''),(54,'kkkk','kkkk'),(55,'ggggg','ggggg'),(56,'hghg','hghg'),(57,'qqq','qqq'),(58,'of','off'),(59,'ffo','ffo'),(60,'mm','mm'),(61,'freategroup','creategroup'),(62,'',''),(63,'',''),(64,'',''),(65,'',''),(66,'',''),(67,'',''),(68,'',''),(69,'name1','n'),(70,'nam','na');
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (5,'euxnfi','hhh','uuuuu'),(4,'eugen','jchchcj','yyyy'),(6,'hfjcc','ryfg','hfjch'),(7,'cata','hdjdh','jfjx'),(8,'eugeeeen','eugen','eugen'),(9,'eugenn','eugen','eugen'),(10,'catalus','catalus','catalus'),(11,'Eugene_fdff','ffff','ffff'),(12,'daniela','danela','daniela'),(13,'','',''),(14,'cata','cata','cata'),(15,'mmmm','mmmm','mmmm'),(16,'hodor','hodor','hodor'),(17,'ex','u','use'),(18,'ma ta','ma ta','ma ta'),(19,'email@email','c','ana'),(20,'c@ex.com','c','ex');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserGroups`
--

DROP TABLE IF EXISTS `UserGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserGroups` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `id_user` int(100) NOT NULL,
  `id_group` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserGroups`
--

LOCK TABLES `UserGroups` WRITE;
/*!40000 ALTER TABLE `UserGroups` DISABLE KEYS */;
INSERT INTO `UserGroups` VALUES (8,10,11),(7,10,10),(6,10,9),(5,10,8),(9,10,12),(99,16,61),(97,15,60),(91,14,56),(94,14,58),(76,12,48),(21,5,24),(85,0,28),(95,14,59),(89,14,55),(92,14,57),(88,14,54),(101,0,62),(102,0,63),(103,0,64),(104,0,65),(105,0,66),(106,0,67),(107,0,68),(109,20,69),(110,20,70),(111,20,70);
/*!40000 ALTER TABLE `UserGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VehicleControl`
--

DROP TABLE IF EXISTS `VehicleControl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VehicleControl` (
  `up` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `down` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `right` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `left` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VehicleControl`
--

LOCK TABLES `VehicleControl` WRITE;
/*!40000 ALTER TABLE `VehicleControl` DISABLE KEYS */;
INSERT INTO `VehicleControl` VALUES ('','','','');
/*!40000 ALTER TABLE `VehicleControl` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-10 19:05:50
