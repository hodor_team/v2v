package com.hodor_ps.hodor_ps_mobile;

public class Coordinates {
    HPoint coordinate;
    String color;// 0 -> Blue, 1 -> Green, 2 -> Red

    public Coordinates() {
        this.coordinate = new HPoint(0,0);
        this.color = "";
    }

    public Coordinates(String color) {
        this.color = color;
    }

    public HPoint getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(HPoint coordinate) {
        this.coordinate = coordinate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String toString() {
        if (coordinate != null)
            return coordinate.x + "," + coordinate.y;
        else
            return "";
    }
}
