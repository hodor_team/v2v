package com.hodor_ps.hodor_ps_mobile;

import org.opencv.core.Point;

public class HPoint extends Point {
    String label;
    public HPoint(String label) {
        super();
        this.label = label;
    }
    public HPoint(String label, double x, double y) {
        super(x, y);
        this.label = label;
    }
    public HPoint(double x, double y) {
        super(x, y);
    }

}
