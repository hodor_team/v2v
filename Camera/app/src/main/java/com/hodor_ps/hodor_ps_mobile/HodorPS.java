package com.hodor_ps.hodor_ps_mobile;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HodorPS {
    public static final int LATIME_SOSEA = 50;
    Field field = new Field();
    Context context;

    public HodorPS(Context context) {
        this.context = context;
    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    public static String POST(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpPost(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    public void eugenRequest(Date timestamp, Coordinates[] coordinates) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://farmacie.3eeweb.com/Cars.php");
        Boolean send = false;
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);

            nameValuePairs.add(new BasicNameValuePair("timestamp", timestamp.toString()));

            if (coordinates[0].coordinate != null) {
                send = true;
                nameValuePairs.add(new BasicNameValuePair("blue", coordinates[0].toString()));
            }
            else nameValuePairs.add(new BasicNameValuePair("blue", ""));
            if (coordinates[1].coordinate != null) {
                send = true;
                nameValuePairs.add(new BasicNameValuePair("green", coordinates[1].toString()));
            }
            else nameValuePairs.add(new BasicNameValuePair("green", ""));
            if (coordinates[2].coordinate != null) {
                send = true;
                nameValuePairs.add(new BasicNameValuePair("red", coordinates[2].toString()));
            }
            else nameValuePairs.add(new BasicNameValuePair("red", ""));

            if (send) {
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Eugen");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpclient.execute(httppost);
            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }

    public Object makeRequest(String path, JSONObject holder) throws Exception {
        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(path);

        //passes the results to a string builder/entity
        StringEntity se = new StringEntity(holder.toString());

        //sets the post request as the resulting string
        httpost.setEntity(se);
        //sets a request header so the page receving the request
        //will know what to do with it
        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json");

        //Handles what is returned from the page
        ResponseHandler responseHandler = new BasicResponseHandler();

        return httpclient.execute(httpost, responseHandler);
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public Latura getLatura(HPoint p) {
        // Caut p.x pe stalpi
        if (Math.abs(p.x - field.getStalp1().get(0).getPunct1().x) < LATIME_SOSEA) {
            // A, E, G, K
            // Caut intervalul y pe stalp 1
            for (Latura l:field.getStalp1()) {
                if (p.y > l.getPunct1().y && p.y < l.getPunct2().y) {
                    // Am gasit latura
                    System.out.println("Latura (AEGK)");
                    return l;
                }
            }
        }

        if (Math.abs(p.x - field.getStalp2().get(0).getPunct1().x) < LATIME_SOSEA) {
            // B, D, H, J
            // Caut intervalul y pe stalp 2
            for (Latura l:field.getStalp2()) {
                if (p.y > l.getPunct1().y && p.y < l.getPunct2().y) {
                    // Am gasit latura
                    System.out.println("Latura (BDHJ)");
                    return l;
                }
            }
        }

        // caut p.y pe laturi
        if (Math.abs(p.y - field.getLatura1().get(0).getPunct1().y) < LATIME_SOSEA) {
            // F, E D, C
            // Caut intervalul x pe latura 1
            for (Latura l:field.getLatura1()) {
                if (p.x > l.getPunct1().x && p.x < l.getPunct2().x) {
                    // Am gasit latura
                    System.out.println("Latura (FEDC)");
                    return l;
                }
            }
        }

        if (Math.abs(p.y - field.getLatura2().get(0).getPunct1().y) < LATIME_SOSEA) {
            // G, H, I
            // Caut intervalul x pe latura 2
            for (Latura l:field.getLatura2()) {
                if (p.x > l.getPunct1().x && p.x < l.getPunct2().x) {
                    // Am gasit latura
                    System.out.println("Latura (GHI)");
                    return l;
                }
            }
        }

        return  null;
    }

    /**
     * Primeste cate 2 puncte succesive de la cele 2 masinute.
     * Primele 2 puncte sunt ale masinutei target.
     * Ultimele 2 sunt coordonatele masinutei care trebuie ghidata catre target
     * @return
     */
    public String tellDirection(HPoint target1, HPoint target2, HPoint src1, HPoint src2) {
        /*
            Prima data trebuie sa gasesc latura pe care se afla fiecare masinuta
         */
        Latura laturaTarget = getLatura(target1);
        Latura laturaTargetNew = getLatura(target2);
        if (!laturaTarget.equals(laturaTargetNew)) {
            // masinuta a trecut intr-o alta latura -> iau urmatorul punct
            System.out.println("A trecut latura!");
        }
        Latura laturaSrc = getLatura(src1);

        assert (laturaTarget != null);
        assert (laturaSrc != null);

        /*
            Am nevoie de directia de deplasare a masinutei target -> nodul catre care se indreapta
         */
        HPoint futureTarget = target1;
        double dif1 = 0, dif2 = 0;

        if (Math.abs(target1.x - target2.x) > LATIME_SOSEA) {
            dif1 = Math.abs(target1.x - laturaTarget.getPunct1().x);
            dif2 = Math.abs(target2.x - laturaTarget.getPunct1().x);
        } else  if (Math.abs(target1.y - target2.y) > LATIME_SOSEA) {
            dif1 = Math.abs(target1.y - laturaTarget.getPunct1().y);
            dif2 = Math.abs(target2.y - laturaTarget.getPunct1().y);
        }

        if (dif1 < dif2) {
            // distanta fata de punct1 este mai mica, deci s-a apropiat
            futureTarget = laturaTarget.getPunct1();
            System.out.println("S-a apropiat de " + futureTarget.label);
        } else {
            // distanta este mai mare, deci s-a apropiat de celalalt punct al laturii
            futureTarget = laturaTarget.getPunct2();
            System.out.println("S-a apropiat de " + futureTarget.label);
        }

        /*
            Acum ca stiu care este futureTarget (Nodul in care va ajunge masinuta coordonator),
            trebuie sa gasesc o modalitate prin care sa-i indic masinutei2 directia in care sa
            se deplaseze.
         */
        HPoint futureSrc = new HPoint(src2.x, src2.y);
        if ((laturaSrc.getPunct1().x - laturaSrc.getPunct2().x) < LATIME_SOSEA) {
            // deplasare pe y pana la futureTarget.y
            futureSrc = new HPoint(src2.x, futureTarget.y);
            System.out.println("Deplaseaza-te pana y-ul punctului " + futureTarget.label);
        } else if  ((laturaSrc.getPunct1().y - laturaSrc.getPunct2().y) < LATIME_SOSEA) {
            // deplasare pe x pana la futureTarget.x
            futureSrc = new HPoint(futureTarget.x, src2.y);
            System.out.println("Deplaseaza-te pana x-ul punctului " + futureTarget.label);
        }

        if (Math.abs(futureSrc.x - futureTarget.x) < LATIME_SOSEA && Math.abs(futureSrc.y - futureTarget.y) < LATIME_SOSEA) {
            //done
            System.out.println("Ai ajuns la detinatie!");
        } else {
            laturaSrc = getLatura(futureSrc);
            if ((laturaSrc.getPunct1().x - laturaSrc.getPunct2().x) < LATIME_SOSEA) {
                // deplasare pe y pana la futureTarget.y
                futureSrc = new HPoint(src2.x, futureTarget.y);
                System.out.println("Deplaseaza-te pana y-ul punctului " + futureTarget.label);
            } else if  ((laturaSrc.getPunct1().y - laturaSrc.getPunct2().y) < LATIME_SOSEA) {
                // deplasare pe x pana la futureTarget.x
                futureSrc = new HPoint(futureTarget.x, src2.y);
                System.out.println("Deplaseaza-te pana x-ul punctului " + futureTarget.label);
            }

            if (Math.abs(futureSrc.x - futureTarget.x) < LATIME_SOSEA && Math.abs(futureSrc.y - futureTarget.y) < LATIME_SOSEA) {
                //done
                System.out.println("Ai ajuns la detinatie!");
            }
        }


        return "";
    }


    private double computeEuclidianDistance(HPoint p1, HPoint p2) {
        return Math.sqrt(Math.pow((p1.x - p2.x), 2) + Math.pow((p1.y - p2.y), 2));
    }

    /**
     * Incerc sa imi dau seama in ce directie se deplaseaza masinuta.
     * Pentru asta am nevoie de 2 puncte succesive de pe traseul acesteia.
     * Presupun ca deplasarea are loc numai paralel cu axele
     *
     * @param p1 - punct timestamp1
     * @param p2 - punct ulterior -> timestamp2
     * @return directia de deplasare
     */
    private String getDirection(HPoint p1, HPoint p2) {
        double xdif = p2.x - p1.x;
        double ydif = p2.y - p1.y;

        if (xdif - 0.5 > 0) {
            return "E";
        } else if (xdif + 0.5 < 0) {
            return "V";
        }

        if (ydif - 0.5 > 0) {
            return "N";
        } else if (ydif + 0.5 < 0) {
            return "S";
        }

        return "";
    }
}
