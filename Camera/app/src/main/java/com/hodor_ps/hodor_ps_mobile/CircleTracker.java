package com.hodor_ps.hodor_ps_mobile;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class CircleTracker {

    private Mat extract_blue_color(Mat input_frame) {
        Mat hsv_frame = new Mat(), output_frame = new Mat();
        Mat mask = new Mat();

        assert input_frame != null;
        Imgproc.cvtColor(input_frame, hsv_frame, Imgproc.COLOR_BGR2HSV);

        Core.inRange(hsv_frame, new Scalar(0, 50, 50), new Scalar(30, 255, 255), mask);
        Core.bitwise_and(input_frame, input_frame, output_frame, mask);

        hsv_frame.release();
        mask.release();

        return output_frame;
    }

    private Mat extract_green_color(Mat input_frame) {
        Mat hsv_frame = new Mat(), output_frame = new Mat();
        Mat mask = new Mat();

        assert input_frame != null;
        Imgproc.cvtColor(input_frame, hsv_frame, Imgproc.COLOR_BGR2HSV);

        Core.inRange(hsv_frame, new Scalar(30, 40, 40), new Scalar(90, 255, 255), mask);
        Core.bitwise_and(input_frame, input_frame, output_frame, mask);

        hsv_frame.release();
        mask.release();

        return output_frame;
    }

    private Mat extract_yellow_color(Mat input_frame) {
        Mat hsv_frame = new Mat(), output_frame = new Mat();
        Mat mask = new Mat();

        assert input_frame != null;
        Imgproc.cvtColor(input_frame, hsv_frame, Imgproc.COLOR_BGR2HSV);

        Core.inRange(hsv_frame, new Scalar(70, 50, 50), new Scalar(110, 255, 255), mask);
        Core.bitwise_and(input_frame, input_frame, output_frame, mask);

        hsv_frame.release();
        mask.release();

        return output_frame;
    }

    private Mat extract_red_color(Mat input_frame) {
        Mat hsv_frame = new Mat(), output_frame = new Mat();
        Mat mask = new Mat();

        assert input_frame != null;
        Imgproc.cvtColor(input_frame, hsv_frame, Imgproc.COLOR_BGR2HSV);

        Core.inRange(hsv_frame, new Scalar(100, 50, 50), new Scalar(169, 255, 255), mask);
        Core.bitwise_and(input_frame, input_frame, output_frame, mask);

        hsv_frame.release();
        mask.release();

        return output_frame;
    }

    private Mat thresholdByColor(Mat input_frame, int channel) {
        Mat clone = input_frame.clone();
        Mat output_frame = new Mat();
        Mat extracted_frame_channel = new Mat();
        Mat extracted_frame_converted = new Mat();

        if (channel == 2) {
            System.out.println("-------------------------- RED -------------------------------");
            Imgproc.cvtColor(extract_red_color(clone), extracted_frame_channel, Imgproc.COLOR_BGR2GRAY);
        }
        else if (channel == 1) {
            System.out.println("-------------------------- GREEN -------------------------------");
            Imgproc.cvtColor(extract_green_color(clone), extracted_frame_channel, Imgproc.COLOR_BGR2GRAY);
        }
        else if (channel == 0) {
            System.out.println("-------------------------- BLUE -------------------------------");
            Imgproc.cvtColor(extract_blue_color(clone), extracted_frame_channel, Imgproc.COLOR_BGR2GRAY);
        }
        extracted_frame_channel.convertTo(extracted_frame_converted, CvType.CV_8UC1);

        Imgproc.erode(extracted_frame_converted, output_frame, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3)));
        Imgproc.dilate(output_frame, output_frame, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5)));

        extracted_frame_channel.release();
        extracted_frame_converted.release();

        return output_frame;
    }

    private Mat getCircles(Mat inputFrame, int channel) {
        Mat threshold_frame = thresholdByColor(inputFrame, channel);
        Mat circles = new Mat();

        int DIST_BETWEEN_CIRCLES = 200;

        double CANNY_EDGE_UPPER_THRESH = 200;
        double CENTER_DETECTION_THRESH = 30;

        int MIN_CIRCLE_RADIUS = 1;
        int MAX_CIRCLE_RADIUS = 100;

        Imgproc.HoughCircles(threshold_frame,
                circles,
                Imgproc.HOUGH_GRADIENT,
                1.0d,
                DIST_BETWEEN_CIRCLES,
                CANNY_EDGE_UPPER_THRESH,
                CENTER_DETECTION_THRESH,
                MIN_CIRCLE_RADIUS,
                MAX_CIRCLE_RADIUS);
        threshold_frame.release();

        return circles;
    }

    private Mat drawCircles(Mat inputFrame, Mat circles, Coordinates coordinate) {
        Mat aux = inputFrame.clone();
        if (circles.cols() > 0) {
            for (int x = 0; x < circles.cols(); x++) {
                double vCircle[] = circles.get(0, x);

                if (vCircle == null)
                    break;

                Point pt = new Point(Math.round(vCircle[0]), Math.round(vCircle[1]));
                HPoint hpt = new HPoint("Cerc" + coordinate.getColor(), pt.x / inputFrame.rows() * 100, pt.y / inputFrame.cols() * 100);
                coordinate.setCoordinate(hpt);
                int radius = (int) Math.round(vCircle[2]);

                Imgproc.circle(aux, pt, radius, new Scalar(0, 255, 255), 7);
                Imgproc.circle(aux, pt, 3, new Scalar(255, 0, 255), 5);
            }
        }
        return aux;
    }

    public Mat process(Mat inputFrame, Coordinates[] coordinates) {
        Mat outputFrame = inputFrame.clone();
        Mat processFrame1 = inputFrame.clone();
        Mat processFrame2 = inputFrame.clone();

        Mat blueCircles = getCircles(inputFrame, 0);
        Mat greenCircles = getCircles(processFrame1, 1);
        Mat redCircles = getCircles(processFrame2, 2);

        outputFrame = drawCircles(outputFrame, blueCircles, coordinates[0]);
        outputFrame = drawCircles(outputFrame, greenCircles, coordinates[1]);
        outputFrame = drawCircles(outputFrame, redCircles, coordinates[2]);

        processFrame1.release();
        processFrame2.release();
        redCircles.release();
        blueCircles.release();
        greenCircles.release();
        inputFrame.release();

        return outputFrame;
    }

}
