package com.hodor_ps.hodor_ps_mobile;

import java.util.ArrayList;
import java.util.Arrays;

public class Field {
    private ArrayList<HPoint> points;

    private ArrayList<Latura> stalp1;
    private ArrayList<Latura> stalp2;

    private ArrayList<Latura> latura1;
    private ArrayList<Latura> latura2;

    HPoint A = new HPoint(10,0);
    HPoint B = new HPoint(90,0);
    HPoint C = new HPoint(100,10);
    HPoint D = new HPoint(90,10);
    HPoint E = new HPoint(10,10);
    HPoint F = new HPoint(0,10);
    HPoint G = new HPoint(10,90);
    HPoint H = new HPoint(90,90);
    HPoint I = new HPoint(100,90);
    HPoint J = new HPoint(90,100);
    HPoint K = new HPoint(10,100);

    public Field() {
        points = new ArrayList<HPoint>(Arrays.asList(A, B, C, D, E, F, G, H, I, J, K));

        stalp2 = new ArrayList<Latura>(
                Arrays.asList(
                        new Latura(B, D),
                        new Latura(D, H),
                        new Latura(H, J)
                        ));
        stalp1 = new ArrayList<Latura>(
                Arrays.asList(
                        new Latura(A, E),
                        new Latura(E, G),
                        new Latura(G, K)
                ));
        latura1 = new ArrayList<Latura>(
                Arrays.asList(
                        new Latura(F, E),
                        new Latura(E, D),
                        new Latura(D, C)
                ));
        latura2 = new ArrayList<Latura>(
                Arrays.asList(
                        new Latura(G, H),
                        new Latura(H, I)
                ));
    }

    public ArrayList<HPoint> getPoints() {
        return points;
    }

    public ArrayList<Latura> getLatura1() {
        return this.latura1;
    }

    public ArrayList<Latura> getStalp1() {
        return stalp1;
    }

    public ArrayList<Latura> getStalp2() {
        return stalp2;
    }

    public ArrayList<Latura> getLatura2() {
        return latura2;
    }

}
