package com.hodor_ps.hodor_ps_mobile;

import org.opencv.core.Point;

public class Latura {
    private HPoint punct1;
    private HPoint punct2;

    public Latura(HPoint punct1, HPoint punct2) {
        this.punct1 = punct1;
        this.punct2 = punct2;
    }

    public HPoint getPunct1() {
        return punct1;
    }

    public HPoint getPunct2() {
        return punct2;
    }

    public String toString() {
        return "Latura : (" + punct1.label + "," + punct2.label + ")";
    }
}
