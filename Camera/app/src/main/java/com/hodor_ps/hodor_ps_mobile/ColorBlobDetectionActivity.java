package com.hodor_ps.hodor_ps_mobile;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;

import java.util.Date;

public class ColorBlobDetectionActivity extends Activity implements OnTouchListener, CvCameraViewListener2 {
    private static final String  TAG              = "OCVSample::Activity";
    private Mat                  mRgba;
    private CircleTracker        circleTracker;
    private CameraBridgeViewBase mOpenCvCameraView;
    private Coordinates[]          lastCoordinates;
    private Coordinates[]          coordinates;
    private HodorPS hodorPS;

    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(ColorBlobDetectionActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public ColorBlobDetectionActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        hodorPS = new HodorPS(this);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.color_blob_detection_surface_view);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.color_blob_detection_activity_surface_view);
        mOpenCvCameraView.setMaxFrameSize(500, 500);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        circleTracker = new CircleTracker();
    }

    public void onCameraViewStopped() {
        if (mRgba != null)
            mRgba.release();
    }

    public boolean onTouch(View v, MotionEvent event) {
        System.out.println("touch");;
        return false;
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        Date timestamp = new Date();
        coordinates = new Coordinates[3];
        coordinates[0] = new Coordinates("blue"); // Blue
        coordinates[1] = new Coordinates("green"); // Green
        coordinates[2] = new Coordinates("red"); // Red
        mRgba = inputFrame.rgba();
        System.out.println("Matrix dims: " + mRgba.cols() + " , " + mRgba.rows());
        lastCoordinates = coordinates.clone();
        Mat resultFrame = circleTracker.process(mRgba, coordinates);
//        hodorPS.eugenRequest(timestamp, coordinates);
        System.out.println(coordinates[0] + "," + coordinates[1] + "," + coordinates[2]);

        if (lastCoordinates[0] != null
                && coordinates[0]!= null
                && lastCoordinates[1]!= null
                && coordinates[1]!= null
                && lastCoordinates[0].getCoordinate() != null
                && lastCoordinates[1].getCoordinate() != null
                && coordinates[0].getCoordinate() != null
                && coordinates[1].getCoordinate() != null) {

            hodorPS.tellDirection(lastCoordinates[0].getCoordinate(), coordinates[0].getCoordinate(),
                                  lastCoordinates[1].getCoordinate(), coordinates[1].getCoordinate());
        } else {
            System.out.println("Nu sunt destule informatii!");
        }
        return resultFrame;
    }
}
